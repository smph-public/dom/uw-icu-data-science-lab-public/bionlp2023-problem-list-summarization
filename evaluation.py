import subprocess
import os
import sys 
def install(package):
    subprocess.check_call([sys.executable, "-m", "pip", "install", package])

install("rouge_score")

import numpy as np 
from rouge_score import rouge_scorer

"""
Applying PyROUGE or SacreROUGE (ROUGE-L)  
"""
def applyPyRouge(gts, preds):
    scorer = rouge_scorer.RougeScorer(['rouge1','rouge2','rougeL'])
    scores = scorer.score(gts, preds) 
    return scores 

    
def compute_rouge(gt, pred):
    """
    Input: data: a Pandas dataframe with two columns: Ground Truth and Prediction
    No need to tokenize 
    """
    scores = [] 
    sc = applyPyRouge(gt, pred)
    #scores.append(sc['rouge-l']['f1'])
    return sc['rougeL'].precision, sc['rougeL'].recall, sc['rougeL'].fmeasure

def score_function(res_file, ref_file):
    # read the input and reference files
    metrics = {}
    precs, recs, f1s = [], [], [] 
    with open(ref_file, 'r') as f_ref_output, open(res_file, 'r') as f_res_output:
        for ref_record, res_record in zip(f_ref_output, f_res_output):
            #print("REF RECORD", ref_record)
            #print("RES RECORD", res_record)
            precision, recall, f1 = compute_rouge(ref_record, res_record)
            precs.append(precision)
            recs.append(recall)
            f1s.append(f1)
    metrics["precision"] = np.mean(precs)
    metrics["recall"] = np.mean(recs)
    metrics["f1"] = np.mean(f1s)

    return metrics 

# res_dir = "/Users/ygao/Desktop/BioNLP/pred_test/system.txt"
# ref_dir = "/Users/ygao/Desktop/BioNLP/CodaLab/reference/answer.txt"
# score = score_function(res_dir, ref_dir)
#print(score)

if __name__ == '__main__':
    # define command-line arguments
    print("Evaluate BioNLP 2023 Shared Task 1A submission file using the ROUGE-L metric")
    """
    Test code
    """
    #res_dir = "/Users/ygao/Desktop/BioNLP/pred_test/system.txt"
    #ref_dir = "/Users/ygao/Desktop/BioNLP/CodaLab/reference/answer.txt"
    #score = score_function(res_dir, ref_dir)
    #print("Scores: ", score)

    #for key, value in score.items():
    #    print(f"{key}:{value}\n")
    """
    For CodaLab scoring program
    """
    _, input_dir, output_dir = sys.argv

    print("Input dir: ", input_dir) 
    print("Output dir: ", output_dir) 
    
    res_dir = os.path.join(input_dir, 'res', "system.txt")
    ref_dir = os.path.join(input_dir, 'ref', "answer.txt")

    if not os.path.isfile(res_dir):
        print("%s doesn't exist" % res_dir)

 
    if os.path.isfile(res_dir) and os.path.isfile(ref_dir):
        if not os.path.exists(output_dir):
            os.makedirs(output_dir)
        score = score_function(res_dir, ref_dir)
        print("Scores: ", score)

        with open(os.path.join(output_dir, "scores.txt"), "w") as output_file:
            for key, value in score.items():
                output_file.write(f"{key}:{value}\n")

        #precision, recall, f1 = score["ROUGE-L Precision"], score["ROUGE-L Recall"], score["ROUGE-L F1"]
        print("Success!")


